﻿namespace WindowsFormsApplication1
{
    partial class SecLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.coms = new System.Windows.Forms.Label();
            this.comment = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.resets = new System.Windows.Forms.Button();
            this.logins = new System.Windows.Forms.Button();
            this.pwdtxtboxs = new System.Windows.Forms.TextBox();
            this.untxtboxs = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // coms
            // 
            this.coms.AutoSize = true;
            this.coms.Location = new System.Drawing.Point(10, 152);
            this.coms.Name = "coms";
            this.coms.Size = new System.Drawing.Size(72, 13);
            this.coms.TabIndex = 17;
            this.coms.Text = "COMMENTS:";
            // 
            // comment
            // 
            this.comment.AutoSize = true;
            this.comment.Location = new System.Drawing.Point(37, 152);
            this.comment.Name = "comment";
            this.comment.Size = new System.Drawing.Size(0, 13);
            this.comment.TabIndex = 16;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label3.Location = new System.Drawing.Point(10, 210);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(354, 13);
            this.label3.TabIndex = 15;
            this.label3.Text = "*Sign Up only done at Administrative Level ie. Direct change in Database.";
            // 
            // resets
            // 
            this.resets.Location = new System.Drawing.Point(189, 115);
            this.resets.Name = "resets";
            this.resets.Size = new System.Drawing.Size(75, 23);
            this.resets.TabIndex = 14;
            this.resets.Text = "Reset";
            this.resets.UseVisualStyleBackColor = true;
            this.resets.Click += new System.EventHandler(this.resets_Click);
            // 
            // logins
            // 
            this.logins.Location = new System.Drawing.Point(75, 115);
            this.logins.Name = "logins";
            this.logins.Size = new System.Drawing.Size(75, 23);
            this.logins.TabIndex = 13;
            this.logins.Text = "Login";
            this.logins.UseVisualStyleBackColor = true;
            this.logins.Click += new System.EventHandler(this.logins_Click);
            // 
            // pwdtxtboxs
            // 
            this.pwdtxtboxs.Location = new System.Drawing.Point(164, 74);
            this.pwdtxtboxs.Name = "pwdtxtboxs";
            this.pwdtxtboxs.Size = new System.Drawing.Size(127, 20);
            this.pwdtxtboxs.TabIndex = 12;
            // 
            // untxtboxs
            // 
            this.untxtboxs.Location = new System.Drawing.Point(164, 37);
            this.untxtboxs.Name = "untxtboxs";
            this.untxtboxs.Size = new System.Drawing.Size(127, 20);
            this.untxtboxs.TabIndex = 11;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(72, 77);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 10;
            this.label2.Text = "Password";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(65, 37);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(60, 13);
            this.label1.TabIndex = 9;
            this.label1.Text = "User Name";
            // 
            // SecLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 234);
            this.Controls.Add(this.coms);
            this.Controls.Add(this.comment);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.resets);
            this.Controls.Add(this.logins);
            this.Controls.Add(this.pwdtxtboxs);
            this.Controls.Add(this.untxtboxs);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "SecLogin";
            this.Text = "SecLogin";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label coms;
        private System.Windows.Forms.Label comment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button resets;
        private System.Windows.Forms.Button logins;
        private System.Windows.Forms.TextBox pwdtxtboxs;
        private System.Windows.Forms.TextBox untxtboxs;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
    }
}