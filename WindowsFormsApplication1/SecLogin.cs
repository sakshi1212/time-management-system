﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using WindowsFormsApplication1.DataModel;

namespace WindowsFormsApplication1
{
   
    public partial class SecLogin : Form
    {
        public SecLogin()
        {
            InitializeComponent();
        }

        private void logins_Click(object sender, EventArgs e)
        {
             string un= untxtboxs.Text;
            string pwd= pwdtxtboxs.Text;
      

            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = @"Data Source=(localdb)\v11.0;Initial Catalog=tmsdb;Integrated Security=True;Pooling=False";
            string loginstring = "SELECT password,type FROM logintable WHERE username ='" + untxtboxs.Text + "'";

            SqlCommand cmd=new SqlCommand(loginstring, conn);
            SqlDataReader reader;
            
            try{
                conn.Open();
                reader=cmd.ExecuteReader();
                reader.Read();
                    
                string chkpwd=Convert.ToString(reader["password"]);
                int type = Convert.ToInt32(reader["type"]);
                //label4.Text = chkpwd + pwd; 
                if (type == 2)
                {
                    if (pwd.Equals(chkpwd))
                    {
                        UserInfo.login = true;
                        UserInfo.uname = un;
                        UserInfo.isExec = false;
                        Close();
                    }
                }
                else
                {
                    coms.Text = "Login not Successfull. Causes may be \n(1) You are employee. \n(2) You do not possess employee id.\n(3) Username password do not match";
                }

                reader.Close();
            }
            catch
            {
                coms.Text = "Exception caught.";
            }
            finally{ conn.Close(); }
            
                 
        }

        private void resets_Click(object sender, EventArgs e)
        {
            untxtboxs.Text = "";
            pwdtxtboxs.Text = "";
            coms.Text = "COMMENTS:";
        }
    }
}
