﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using WindowsFormsApplication1.DataModel;

namespace WindowsFormsApplication1
{   
    public partial class ExecLogin : Form
    {
        public ExecLogin()
        {
            InitializeComponent();
        }

        private void ExecLogin_Load(object sender, EventArgs e)
        {

        }

        private void login_Click(object sender, EventArgs e)
        {
            string un = untxtbox.Text;
            string pwd = pwdtxtbox.Text;
          
            SqlConnection conn = new SqlConnection();
            conn.ConnectionString = @"Data Source=(localdb)\v11.0;Initial Catalog=tmsdb;Integrated Security=True;Pooling=False";
            string loginstring = "SELECT password,type FROM logintable WHERE username ='" + untxtbox.Text + "'";

            SqlCommand cmd = new SqlCommand(loginstring, conn);
            SqlDataReader reader;

            try
            {
                conn.Open();
                reader = cmd.ExecuteReader();
                reader.Read();

                string chkpwd = Convert.ToString(reader["password"]);
                int type = Convert.ToInt32(reader["type"]);
                
                if (type == 1)
                {
                    if (pwd.Equals(chkpwd))
                    {
                        
                        UserInfo.login = true;
                        UserInfo.uname = un;
                        UserInfo.isExec = true;
                        Close();
                        
                    }
                }
                else
                {
                    com.Text = "Login not Successfull. Causes may be \n(1) You are secretary. \n(2) You do not possess employee id.\n(3) Username password do not match";
                }

                reader.Close();
            }
            catch
            {
                com.Text = "Exception caught.";
            }
            finally { conn.Close(); }

            /*if ((chkpwd.ToString()).CompareTo(pwd)==0)
            {
                Success success1 = new Success();
                success1.ShowDialog();
            }*/

        }

        private void reset_Click(object sender, EventArgs e)
        {
            untxtbox.Text = "";
            pwdtxtbox.Text = "";
            com.Text = "COMMENTS:";
        }
    }
}
