﻿namespace WindowsFormsApplication1
{
    partial class TimeMgmtSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(TimeMgmtSystem));
            this.panel1 = new System.Windows.Forms.Panel();
            this.welcome = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.logout = new System.Windows.Forms.Button();
            this.seclogin = new System.Windows.Forms.Button();
            this.exelogin = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.Home = new System.Windows.Forms.TabPage();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.RegApp = new System.Windows.Forms.TabPage();
            this.tabControl2 = new System.Windows.Forms.TabControl();
            this.tabPage6 = new System.Windows.Forms.TabPage();
            this.arrangebutton = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ettxtbox = new System.Windows.Forms.TextBox();
            this.sttxtbox = new System.Windows.Forms.TextBox();
            this.appnumlabel = new System.Windows.Forms.Label();
            this.button5 = new System.Windows.Forms.Button();
            this.ppltxtbox = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.venuetxtbox = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.appdate = new System.Windows.Forms.DateTimePicker();
            this.label2 = new System.Windows.Forms.Label();
            this.tabPage7 = new System.Windows.Forms.TabPage();
            this.button7 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.tabPage8 = new System.Windows.Forms.TabPage();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.Arrange = new System.Windows.Forms.TabPage();
            this.progressBar1 = new System.Windows.Forms.ProgressBar();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.ChkSch = new System.Windows.Forms.TabPage();
            this.button10 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.monthCalendar1 = new System.Windows.Forms.MonthCalendar();
            this.label16 = new System.Windows.Forms.Label();
            this.Update = new System.Windows.Forms.TabPage();
            this.tabControl3 = new System.Windows.Forms.TabControl();
            this.tabPage9 = new System.Windows.Forms.TabPage();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.submitbutton = new System.Windows.Forms.Button();
            this.leaveenddate = new System.Windows.Forms.MonthCalendar();
            this.leavestartdate = new System.Windows.Forms.MonthCalendar();
            this.tabPage10 = new System.Windows.Forms.TabPage();
            this.label21 = new System.Windows.Forms.Label();
            this.button12 = new System.Windows.Forms.Button();
            this.dateTimePicker2 = new System.Windows.Forms.DateTimePicker();
            this.label20 = new System.Windows.Forms.Label();
            this.tabPage11 = new System.Windows.Forms.TabPage();
            this.button13 = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.button14 = new System.Windows.Forms.Button();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.Stat = new System.Windows.Forms.TabPage();
            this.tabControl4 = new System.Windows.Forms.TabControl();
            this.tabPage12 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tabPage13 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            this.tabPage14 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tabPage15 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.Slot = new System.Windows.Forms.TabPage();
            this.label31 = new System.Windows.Forms.Label();
            this.button16 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.label30 = new System.Windows.Forms.Label();
            this.numericUpDown5 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown6 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown7 = new System.Windows.Forms.NumericUpDown();
            this.numericUpDown8 = new System.Windows.Forms.NumericUpDown();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.dateTimePicker3 = new System.Windows.Forms.DateTimePicker();
            this.textBox3 = new System.Windows.Forms.TextBox();
            this.label24 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.Home.SuspendLayout();
            this.RegApp.SuspendLayout();
            this.tabControl2.SuspendLayout();
            this.tabPage6.SuspendLayout();
            this.tabPage7.SuspendLayout();
            this.tabPage8.SuspendLayout();
            this.Arrange.SuspendLayout();
            this.ChkSch.SuspendLayout();
            this.Update.SuspendLayout();
            this.tabControl3.SuspendLayout();
            this.tabPage9.SuspendLayout();
            this.tabPage10.SuspendLayout();
            this.tabPage11.SuspendLayout();
            this.Stat.SuspendLayout();
            this.tabControl4.SuspendLayout();
            this.tabPage12.SuspendLayout();
            this.tabPage13.SuspendLayout();
            this.tabPage14.SuspendLayout();
            this.tabPage15.SuspendLayout();
            this.Slot.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panel1.Controls.Add(this.welcome);
            this.panel1.Location = new System.Drawing.Point(12, 12);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(708, 37);
            this.panel1.TabIndex = 0;
            // 
            // welcome
            // 
            this.welcome.AutoSize = true;
            this.welcome.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.welcome.ForeColor = System.Drawing.Color.White;
            this.welcome.Location = new System.Drawing.Point(275, 9);
            this.welcome.Name = "welcome";
            this.welcome.Size = new System.Drawing.Size(132, 17);
            this.welcome.TabIndex = 0;
            this.welcome.Text = "WELCOME USER";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
            this.panel2.Controls.Add(this.logout);
            this.panel2.Controls.Add(this.seclogin);
            this.panel2.Controls.Add(this.exelogin);
            this.panel2.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
            this.panel2.Location = new System.Drawing.Point(12, 55);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(126, 429);
            this.panel2.TabIndex = 1;
            this.panel2.Paint += new System.Windows.Forms.PaintEventHandler(this.panel2_Paint);
            // 
            // logout
            // 
            this.logout.Location = new System.Drawing.Point(16, 349);
            this.logout.Name = "logout";
            this.logout.Size = new System.Drawing.Size(92, 29);
            this.logout.TabIndex = 2;
            this.logout.Text = "LOGOUT";
            this.logout.UseVisualStyleBackColor = true;
            // 
            // seclogin
            // 
            this.seclogin.Location = new System.Drawing.Point(16, 152);
            this.seclogin.Name = "seclogin";
            this.seclogin.Size = new System.Drawing.Size(92, 39);
            this.seclogin.TabIndex = 1;
            this.seclogin.Text = "SECRETARY LOGIN";
            this.seclogin.UseVisualStyleBackColor = true;
            this.seclogin.Click += new System.EventHandler(this.button2_Click);
            // 
            // exelogin
            // 
            this.exelogin.Location = new System.Drawing.Point(16, 80);
            this.exelogin.Name = "exelogin";
            this.exelogin.Size = new System.Drawing.Size(92, 36);
            this.exelogin.TabIndex = 0;
            this.exelogin.Text = "EXECUTIVE LOGIN";
            this.exelogin.UseVisualStyleBackColor = true;
            this.exelogin.Click += new System.EventHandler(this.button1_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.Home);
            this.tabControl1.Controls.Add(this.RegApp);
            this.tabControl1.Controls.Add(this.Arrange);
            this.tabControl1.Controls.Add(this.ChkSch);
            this.tabControl1.Controls.Add(this.Update);
            this.tabControl1.Controls.Add(this.Stat);
            this.tabControl1.Controls.Add(this.Slot);
            this.tabControl1.Location = new System.Drawing.Point(144, 55);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(586, 429);
            this.tabControl1.TabIndex = 2;
            // 
            // Home
            // 
            this.Home.Controls.Add(this.textBox2);
            this.Home.Controls.Add(this.label23);
            this.Home.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Home.Location = new System.Drawing.Point(4, 22);
            this.Home.Name = "Home";
            this.Home.Size = new System.Drawing.Size(578, 403);
            this.Home.TabIndex = 5;
            this.Home.Text = "Home";
            this.Home.UseVisualStyleBackColor = true;
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.SystemColors.ControlLight;
            this.textBox2.Enabled = false;
            this.textBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.ForeColor = System.Drawing.Color.Black;
            this.textBox2.Location = new System.Drawing.Point(89, 130);
            this.textBox2.Multiline = true;
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(393, 90);
            this.textBox2.TabIndex = 1;
            this.textBox2.Text = resources.GetString("textBox2.Text");
            this.textBox2.TextChanged += new System.EventHandler(this.textBox2_TextChanged);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(69, 47);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(0, 13);
            this.label23.TabIndex = 0;
            // 
            // RegApp
            // 
            this.RegApp.AutoScroll = true;
            this.RegApp.Controls.Add(this.tabControl2);
            this.RegApp.Location = new System.Drawing.Point(4, 22);
            this.RegApp.Name = "RegApp";
            this.RegApp.Padding = new System.Windows.Forms.Padding(3);
            this.RegApp.Size = new System.Drawing.Size(578, 403);
            this.RegApp.TabIndex = 0;
            this.RegApp.Text = "Register Appointments";
            this.RegApp.UseVisualStyleBackColor = true;
            this.RegApp.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // tabControl2
            // 
            this.tabControl2.Controls.Add(this.tabPage6);
            this.tabControl2.Controls.Add(this.tabPage7);
            this.tabControl2.Controls.Add(this.tabPage8);
            this.tabControl2.Location = new System.Drawing.Point(6, 6);
            this.tabControl2.Name = "tabControl2";
            this.tabControl2.SelectedIndex = 0;
            this.tabControl2.Size = new System.Drawing.Size(569, 394);
            this.tabControl2.TabIndex = 0;
            // 
            // tabPage6
            // 
            this.tabPage6.Controls.Add(this.arrangebutton);
            this.tabPage6.Controls.Add(this.label5);
            this.tabPage6.Controls.Add(this.ettxtbox);
            this.tabPage6.Controls.Add(this.sttxtbox);
            this.tabPage6.Controls.Add(this.appnumlabel);
            this.tabPage6.Controls.Add(this.button5);
            this.tabPage6.Controls.Add(this.ppltxtbox);
            this.tabPage6.Controls.Add(this.label8);
            this.tabPage6.Controls.Add(this.venuetxtbox);
            this.tabPage6.Controls.Add(this.label9);
            this.tabPage6.Controls.Add(this.label7);
            this.tabPage6.Controls.Add(this.label4);
            this.tabPage6.Controls.Add(this.label3);
            this.tabPage6.Controls.Add(this.appdate);
            this.tabPage6.Controls.Add(this.label2);
            this.tabPage6.Location = new System.Drawing.Point(4, 22);
            this.tabPage6.Name = "tabPage6";
            this.tabPage6.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage6.Size = new System.Drawing.Size(561, 368);
            this.tabPage6.TabIndex = 0;
            this.tabPage6.Text = "Create Apppointment";
            this.tabPage6.UseVisualStyleBackColor = true;
            // 
            // arrangebutton
            // 
            this.arrangebutton.Location = new System.Drawing.Point(89, 325);
            this.arrangebutton.Name = "arrangebutton";
            this.arrangebutton.Size = new System.Drawing.Size(75, 23);
            this.arrangebutton.TabIndex = 42;
            this.arrangebutton.Text = "Arrange";
            this.arrangebutton.UseVisualStyleBackColor = true;
            this.arrangebutton.Click += new System.EventHandler(this.arrangebutton_Click_1);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(245, 275);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(267, 13);
            this.label5.TabIndex = 41;
            this.label5.Text = "*If one on one meeting with client, leave textbox empty.";
            // 
            // ettxtbox
            // 
            this.ettxtbox.Location = new System.Drawing.Point(248, 131);
            this.ettxtbox.Name = "ettxtbox";
            this.ettxtbox.Size = new System.Drawing.Size(43, 20);
            this.ettxtbox.TabIndex = 40;
            // 
            // sttxtbox
            // 
            this.sttxtbox.Location = new System.Drawing.Point(248, 85);
            this.sttxtbox.Name = "sttxtbox";
            this.sttxtbox.Size = new System.Drawing.Size(43, 20);
            this.sttxtbox.TabIndex = 39;
            // 
            // appnumlabel
            // 
            this.appnumlabel.AutoSize = true;
            this.appnumlabel.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.appnumlabel.Location = new System.Drawing.Point(320, 330);
            this.appnumlabel.Name = "appnumlabel";
            this.appnumlabel.Size = new System.Drawing.Size(209, 13);
            this.appnumlabel.TabIndex = 38;
            this.appnumlabel.Text = "Appointment Number will be displayed here";
            this.appnumlabel.Click += new System.EventHandler(this.label12_Click);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(216, 325);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(75, 23);
            this.button5.TabIndex = 37;
            this.button5.Text = "Reset";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.button5_Click);
            // 
            // ppltxtbox
            // 
            this.ppltxtbox.Location = new System.Drawing.Point(248, 217);
            this.ppltxtbox.Multiline = true;
            this.ppltxtbox.Name = "ppltxtbox";
            this.ppltxtbox.Size = new System.Drawing.Size(237, 43);
            this.ppltxtbox.TabIndex = 35;
            this.ppltxtbox.Text = "Enter names seperated with colon;";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(100, 217);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(40, 13);
            this.label8.TabIndex = 32;
            this.label8.Text = "People";
            // 
            // venuetxtbox
            // 
            this.venuetxtbox.Location = new System.Drawing.Point(248, 173);
            this.venuetxtbox.Name = "venuetxtbox";
            this.venuetxtbox.Size = new System.Drawing.Size(237, 20);
            this.venuetxtbox.TabIndex = 31;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(97, 173);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(38, 13);
            this.label9.TabIndex = 30;
            this.label9.Text = "Venue";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(97, 131);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(80, 13);
            this.label7.TabIndex = 26;
            this.label7.Text = "Enter End Time";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(97, 93);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(83, 13);
            this.label4.TabIndex = 22;
            this.label4.Text = "Enter Start Time";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(97, 55);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(137, 13);
            this.label3.TabIndex = 21;
            this.label3.Text = "Select Date of Appointment";
            // 
            // appdate
            // 
            this.appdate.Location = new System.Drawing.Point(248, 49);
            this.appdate.Name = "appdate";
            this.appdate.Size = new System.Drawing.Size(237, 20);
            this.appdate.TabIndex = 20;
            this.appdate.ValueChanged += new System.EventHandler(this.appdate_ValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(76, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(273, 13);
            this.label2.TabIndex = 19;
            this.label2.Text = "Enter the following information to register an appointment";
            // 
            // tabPage7
            // 
            this.tabPage7.Controls.Add(this.button7);
            this.tabPage7.Controls.Add(this.button6);
            this.tabPage7.Controls.Add(this.textBox7);
            this.tabPage7.Controls.Add(this.label10);
            this.tabPage7.Location = new System.Drawing.Point(4, 22);
            this.tabPage7.Name = "tabPage7";
            this.tabPage7.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage7.Size = new System.Drawing.Size(561, 368);
            this.tabPage7.TabIndex = 1;
            this.tabPage7.Text = "Edit Appointment";
            this.tabPage7.UseVisualStyleBackColor = true;
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(303, 115);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(75, 23);
            this.button7.TabIndex = 3;
            this.button7.Text = "Reset";
            this.button7.UseVisualStyleBackColor = true;
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(132, 115);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(75, 23);
            this.button6.TabIndex = 2;
            this.button6.Text = "Edit";
            this.button6.UseVisualStyleBackColor = true;
            // 
            // textBox7
            // 
            this.textBox7.Location = new System.Drawing.Point(303, 52);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(128, 20);
            this.textBox7.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(60, 52);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(218, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Enter Appointment Reference number to Edit";
            this.label10.Click += new System.EventHandler(this.label10_Click);
            // 
            // tabPage8
            // 
            this.tabPage8.Controls.Add(this.button8);
            this.tabPage8.Controls.Add(this.button9);
            this.tabPage8.Controls.Add(this.textBox8);
            this.tabPage8.Controls.Add(this.label11);
            this.tabPage8.Location = new System.Drawing.Point(4, 22);
            this.tabPage8.Name = "tabPage8";
            this.tabPage8.Size = new System.Drawing.Size(561, 368);
            this.tabPage8.TabIndex = 2;
            this.tabPage8.Text = "Cancel Appointment";
            this.tabPage8.UseVisualStyleBackColor = true;
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(327, 140);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(75, 23);
            this.button8.TabIndex = 7;
            this.button8.Text = "Reset";
            this.button8.UseVisualStyleBackColor = true;
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(156, 140);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(75, 23);
            this.button9.TabIndex = 6;
            this.button9.Text = "Cancel";
            this.button9.UseVisualStyleBackColor = true;
            // 
            // textBox8
            // 
            this.textBox8.Location = new System.Drawing.Point(327, 77);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(128, 20);
            this.textBox8.TabIndex = 5;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(84, 77);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(233, 13);
            this.label11.TabIndex = 4;
            this.label11.Text = "Enter Appointment Reference number to Cancel";
            // 
            // Arrange
            // 
            this.Arrange.Controls.Add(this.progressBar1);
            this.Arrange.Controls.Add(this.textBox10);
            this.Arrange.Controls.Add(this.label15);
            this.Arrange.Controls.Add(this.label14);
            this.Arrange.Controls.Add(this.textBox9);
            this.Arrange.Controls.Add(this.label13);
            this.Arrange.Location = new System.Drawing.Point(4, 22);
            this.Arrange.Name = "Arrange";
            this.Arrange.Padding = new System.Windows.Forms.Padding(3);
            this.Arrange.Size = new System.Drawing.Size(578, 403);
            this.Arrange.TabIndex = 1;
            this.Arrange.Text = "Arrange Meetings";
            this.Arrange.UseVisualStyleBackColor = true;
            // 
            // progressBar1
            // 
            this.progressBar1.Location = new System.Drawing.Point(238, 175);
            this.progressBar1.Name = "progressBar1";
            this.progressBar1.Size = new System.Drawing.Size(100, 23);
            this.progressBar1.TabIndex = 5;
            // 
            // textBox10
            // 
            this.textBox10.Location = new System.Drawing.Point(134, 282);
            this.textBox10.Multiline = true;
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(289, 74);
            this.textBox10.TabIndex = 4;
            this.textBox10.Text = "// if succesfull, appointment added to schedule, email sent to executive and secr" +
    "etary____; if unsussessfull, email sent to secretary to find time slot";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(134, 254);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(56, 13);
            this.label15.TabIndex = 3;
            this.label15.Text = "Comments";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(131, 216);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(272, 13);
            this.label14.TabIndex = 2;
            this.label14.Text = "Label will display if registration succesful or unsuccessful";
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(95, 58);
            this.textBox9.Multiline = true;
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(390, 111);
            this.textBox9.TabIndex = 1;
            this.textBox9.Text = "//details taken from Register appointments tab";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(58, 30);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(171, 13);
            this.label13.TabIndex = 0;
            this.label13.Text = "Meeting Details as per Registration";
            // 
            // ChkSch
            // 
            this.ChkSch.Controls.Add(this.button10);
            this.ChkSch.Controls.Add(this.tableLayoutPanel1);
            this.ChkSch.Controls.Add(this.monthCalendar1);
            this.ChkSch.Controls.Add(this.label16);
            this.ChkSch.Location = new System.Drawing.Point(4, 22);
            this.ChkSch.Name = "ChkSch";
            this.ChkSch.Size = new System.Drawing.Size(578, 403);
            this.ChkSch.TabIndex = 2;
            this.ChkSch.Text = "Check Schedule";
            this.ChkSch.UseVisualStyleBackColor = true;
            this.ChkSch.Click += new System.EventHandler(this.tabPage3_Click);
            // 
            // button10
            // 
            this.button10.Location = new System.Drawing.Point(69, 241);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(135, 27);
            this.button10.TabIndex = 25;
            this.button10.Text = "Display Schedule";
            this.button10.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel1.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.25551F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 65.74449F));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(278, 40);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 16;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(257, 323);
            this.tableLayoutPanel1.TabIndex = 24;
            // 
            // monthCalendar1
            // 
            this.monthCalendar1.Location = new System.Drawing.Point(25, 40);
            this.monthCalendar1.Name = "monthCalendar1";
            this.monthCalendar1.TabIndex = 23;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(22, 18);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(63, 13);
            this.label16.TabIndex = 22;
            this.label16.Text = "Select Date";
            // 
            // Update
            // 
            this.Update.Controls.Add(this.tabControl3);
            this.Update.Location = new System.Drawing.Point(4, 22);
            this.Update.Name = "Update";
            this.Update.Size = new System.Drawing.Size(578, 403);
            this.Update.TabIndex = 3;
            this.Update.Text = "Update Diary";
            this.Update.UseVisualStyleBackColor = true;
            // 
            // tabControl3
            // 
            this.tabControl3.Controls.Add(this.tabPage9);
            this.tabControl3.Controls.Add(this.tabPage10);
            this.tabControl3.Controls.Add(this.tabPage11);
            this.tabControl3.Location = new System.Drawing.Point(4, 4);
            this.tabControl3.Name = "tabControl3";
            this.tabControl3.SelectedIndex = 0;
            this.tabControl3.Size = new System.Drawing.Size(571, 396);
            this.tabControl3.TabIndex = 0;
            // 
            // tabPage9
            // 
            this.tabPage9.Controls.Add(this.label19);
            this.tabPage9.Controls.Add(this.label18);
            this.tabPage9.Controls.Add(this.label17);
            this.tabPage9.Controls.Add(this.submitbutton);
            this.tabPage9.Controls.Add(this.leaveenddate);
            this.tabPage9.Controls.Add(this.leavestartdate);
            this.tabPage9.Location = new System.Drawing.Point(4, 22);
            this.tabPage9.Name = "tabPage9";
            this.tabPage9.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage9.Size = new System.Drawing.Size(563, 370);
            this.tabPage9.TabIndex = 0;
            this.tabPage9.Text = "Mark Leave";
            this.tabPage9.UseVisualStyleBackColor = true;
            this.tabPage9.Click += new System.EventHandler(this.tabPage9_Click);
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(294, 68);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(116, 13);
            this.label19.TabIndex = 5;
            this.label19.Text = "Enter Leave End Date:";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(33, 68);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(119, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "Enter Leave Start Date:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label17.Location = new System.Drawing.Point(270, 311);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(233, 13);
            this.label17.TabIndex = 3;
            this.label17.Text = "Leave Reference Number will be displayed here";
            // 
            // submitbutton
            // 
            this.submitbutton.Location = new System.Drawing.Point(166, 301);
            this.submitbutton.Name = "submitbutton";
            this.submitbutton.Size = new System.Drawing.Size(75, 23);
            this.submitbutton.TabIndex = 2;
            this.submitbutton.Text = "Submit";
            this.submitbutton.UseVisualStyleBackColor = true;
            // 
            // leaveenddate
            // 
            this.leaveenddate.Location = new System.Drawing.Point(297, 104);
            this.leaveenddate.Name = "leaveenddate";
            this.leaveenddate.TabIndex = 1;
            // 
            // leavestartdate
            // 
            this.leavestartdate.Location = new System.Drawing.Point(36, 104);
            this.leavestartdate.Name = "leavestartdate";
            this.leavestartdate.TabIndex = 0;
            // 
            // tabPage10
            // 
            this.tabPage10.Controls.Add(this.label21);
            this.tabPage10.Controls.Add(this.button12);
            this.tabPage10.Controls.Add(this.dateTimePicker2);
            this.tabPage10.Controls.Add(this.label20);
            this.tabPage10.Location = new System.Drawing.Point(4, 22);
            this.tabPage10.Name = "tabPage10";
            this.tabPage10.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage10.Size = new System.Drawing.Size(563, 370);
            this.tabPage10.TabIndex = 1;
            this.tabPage10.Text = "Mark Important Job";
            this.tabPage10.UseVisualStyleBackColor = true;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label21.Location = new System.Drawing.Point(232, 117);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(267, 13);
            this.label21.TabIndex = 4;
            this.label21.Text = "Important Job Reference Number will be displayed here";
            // 
            // button12
            // 
            this.button12.Location = new System.Drawing.Point(103, 112);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(75, 23);
            this.button12.TabIndex = 2;
            this.button12.Text = "Submit";
            this.button12.UseVisualStyleBackColor = true;
            // 
            // dateTimePicker2
            // 
            this.dateTimePicker2.Location = new System.Drawing.Point(235, 54);
            this.dateTimePicker2.Name = "dateTimePicker2";
            this.dateTimePicker2.Size = new System.Drawing.Size(200, 20);
            this.dateTimePicker2.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(55, 54);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(137, 13);
            this.label20.TabIndex = 0;
            this.label20.Text = "Enter date for important job:";
            // 
            // tabPage11
            // 
            this.tabPage11.Controls.Add(this.button13);
            this.tabPage11.Controls.Add(this.label22);
            this.tabPage11.Controls.Add(this.button14);
            this.tabPage11.Controls.Add(this.textBox1);
            this.tabPage11.Location = new System.Drawing.Point(4, 22);
            this.tabPage11.Name = "tabPage11";
            this.tabPage11.Size = new System.Drawing.Size(563, 370);
            this.tabPage11.TabIndex = 2;
            this.tabPage11.Text = "Cancel Leave or Important Job";
            this.tabPage11.UseVisualStyleBackColor = true;
            this.tabPage11.Click += new System.EventHandler(this.tabPage11_Click);
            // 
            // button13
            // 
            this.button13.Location = new System.Drawing.Point(321, 147);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(75, 23);
            this.button13.TabIndex = 11;
            this.button13.Text = "Reset";
            this.button13.UseVisualStyleBackColor = true;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(35, 87);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(280, 13);
            this.label22.TabIndex = 8;
            this.label22.Text = "Enter Leave or Important job Reference number to Cancel";
            // 
            // button14
            // 
            this.button14.Location = new System.Drawing.Point(150, 147);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(75, 23);
            this.button14.TabIndex = 10;
            this.button14.Text = "Cancel";
            this.button14.UseVisualStyleBackColor = true;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(321, 84);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(128, 20);
            this.textBox1.TabIndex = 9;
            // 
            // Stat
            // 
            this.Stat.Controls.Add(this.tabControl4);
            this.Stat.Location = new System.Drawing.Point(4, 22);
            this.Stat.Name = "Stat";
            this.Stat.Size = new System.Drawing.Size(578, 403);
            this.Stat.TabIndex = 4;
            this.Stat.Text = "Statistics";
            this.Stat.UseVisualStyleBackColor = true;
            // 
            // tabControl4
            // 
            this.tabControl4.Controls.Add(this.tabPage12);
            this.tabControl4.Controls.Add(this.tabPage13);
            this.tabControl4.Controls.Add(this.tabPage14);
            this.tabControl4.Controls.Add(this.tabPage15);
            this.tabControl4.Location = new System.Drawing.Point(4, 4);
            this.tabControl4.Name = "tabControl4";
            this.tabControl4.SelectedIndex = 0;
            this.tabControl4.Size = new System.Drawing.Size(571, 396);
            this.tabControl4.TabIndex = 0;
            // 
            // tabPage12
            // 
            this.tabPage12.Controls.Add(this.tableLayoutPanel2);
            this.tabPage12.Location = new System.Drawing.Point(4, 22);
            this.tabPage12.Name = "tabPage12";
            this.tabPage12.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage12.Size = new System.Drawing.Size(563, 370);
            this.tabPage12.TabIndex = 0;
            this.tabPage12.Text = "Time/Executive";
            this.tabPage12.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel2.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(53, 32);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 10;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(468, 298);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tabPage13
            // 
            this.tabPage13.Controls.Add(this.tableLayoutPanel3);
            this.tabPage13.Controls.Add(this.webBrowser1);
            this.tabPage13.Location = new System.Drawing.Point(4, 22);
            this.tabPage13.Name = "tabPage13";
            this.tabPage13.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage13.Size = new System.Drawing.Size(563, 370);
            this.tabPage13.TabIndex = 1;
            this.tabPage13.Text = "Meetings/Project";
            this.tabPage13.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel3.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(47, 36);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 10;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(468, 298);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // webBrowser1
            // 
            this.webBrowser1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.webBrowser1.Location = new System.Drawing.Point(3, 3);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(557, 364);
            this.webBrowser1.TabIndex = 0;
            // 
            // tabPage14
            // 
            this.tabPage14.Controls.Add(this.tableLayoutPanel4);
            this.tabPage14.Location = new System.Drawing.Point(4, 22);
            this.tabPage14.Name = "tabPage14";
            this.tabPage14.Size = new System.Drawing.Size(563, 370);
            this.tabPage14.TabIndex = 2;
            this.tabPage14.Text = "Meeting Hours/Executive";
            this.tabPage14.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel4.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel4.Location = new System.Drawing.Point(47, 36);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 10;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(468, 298);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // tabPage15
            // 
            this.tabPage15.Controls.Add(this.tableLayoutPanel5);
            this.tabPage15.Location = new System.Drawing.Point(4, 22);
            this.tabPage15.Name = "tabPage15";
            this.tabPage15.Size = new System.Drawing.Size(563, 370);
            this.tabPage15.TabIndex = 3;
            this.tabPage15.Text = "Average Fraction of Total Time/Executive";
            this.tabPage15.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.BackColor = System.Drawing.Color.LightGray;
            this.tableLayoutPanel5.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Single;
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel5.Location = new System.Drawing.Point(47, 36);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 10;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 10F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(468, 298);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // Slot
            // 
            this.Slot.Controls.Add(this.label31);
            this.Slot.Controls.Add(this.button16);
            this.Slot.Controls.Add(this.button17);
            this.Slot.Controls.Add(this.label30);
            this.Slot.Controls.Add(this.numericUpDown5);
            this.Slot.Controls.Add(this.numericUpDown6);
            this.Slot.Controls.Add(this.numericUpDown7);
            this.Slot.Controls.Add(this.numericUpDown8);
            this.Slot.Controls.Add(this.label25);
            this.Slot.Controls.Add(this.label26);
            this.Slot.Controls.Add(this.label27);
            this.Slot.Controls.Add(this.label28);
            this.Slot.Controls.Add(this.label29);
            this.Slot.Controls.Add(this.dateTimePicker3);
            this.Slot.Controls.Add(this.textBox3);
            this.Slot.Controls.Add(this.label24);
            this.Slot.Location = new System.Drawing.Point(4, 22);
            this.Slot.Name = "Slot";
            this.Slot.Size = new System.Drawing.Size(578, 403);
            this.Slot.TabIndex = 6;
            this.Slot.Text = "Time Slot";
            this.Slot.UseVisualStyleBackColor = true;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.label31.Location = new System.Drawing.Point(340, 321);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(209, 13);
            this.label31.TabIndex = 57;
            this.label31.Text = "Appointment Number will be displayed here";
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(233, 311);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(75, 23);
            this.button16.TabIndex = 56;
            this.button16.Text = "Reset";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // button17
            // 
            this.button17.Location = new System.Drawing.Point(108, 311);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(75, 23);
            this.button17.TabIndex = 55;
            this.button17.Text = "Arrange";
            this.button17.UseVisualStyleBackColor = true;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Location = new System.Drawing.Point(67, 58);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(276, 13);
            this.label30.TabIndex = 53;
            this.label30.Text = "Select a new Time slot and new Date for an Appointment";
            // 
            // numericUpDown5
            // 
            this.numericUpDown5.Location = new System.Drawing.Point(314, 245);
            this.numericUpDown5.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown5.Name = "numericUpDown5";
            this.numericUpDown5.Size = new System.Drawing.Size(36, 20);
            this.numericUpDown5.TabIndex = 52;
            this.numericUpDown5.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // numericUpDown6
            // 
            this.numericUpDown6.Location = new System.Drawing.Point(314, 209);
            this.numericUpDown6.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numericUpDown6.Name = "numericUpDown6";
            this.numericUpDown6.Size = new System.Drawing.Size(36, 20);
            this.numericUpDown6.TabIndex = 51;
            this.numericUpDown6.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // numericUpDown7
            // 
            this.numericUpDown7.Location = new System.Drawing.Point(256, 245);
            this.numericUpDown7.Maximum = new decimal(new int[] {
            17,
            0,
            0,
            0});
            this.numericUpDown7.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDown7.Name = "numericUpDown7";
            this.numericUpDown7.Size = new System.Drawing.Size(36, 20);
            this.numericUpDown7.TabIndex = 50;
            this.numericUpDown7.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // numericUpDown8
            // 
            this.numericUpDown8.Location = new System.Drawing.Point(256, 209);
            this.numericUpDown8.Maximum = new decimal(new int[] {
            16,
            0,
            0,
            0});
            this.numericUpDown8.Minimum = new decimal(new int[] {
            8,
            0,
            0,
            0});
            this.numericUpDown8.Name = "numericUpDown8";
            this.numericUpDown8.Size = new System.Drawing.Size(36, 20);
            this.numericUpDown8.TabIndex = 49;
            this.numericUpDown8.Value = new decimal(new int[] {
            8,
            0,
            0,
            0});
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(298, 249);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(10, 13);
            this.label25.TabIndex = 48;
            this.label25.Text = ":";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(105, 247);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 13);
            this.label26.TabIndex = 47;
            this.label26.Text = "Enter End Time";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Location = new System.Drawing.Point(298, 211);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(10, 13);
            this.label27.TabIndex = 46;
            this.label27.Text = ":";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Location = new System.Drawing.Point(105, 209);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(83, 13);
            this.label28.TabIndex = 45;
            this.label28.Text = "Enter Start Time";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Location = new System.Drawing.Point(105, 171);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(137, 13);
            this.label29.TabIndex = 44;
            this.label29.Text = "Select Date of Appointment";
            // 
            // dateTimePicker3
            // 
            this.dateTimePicker3.Location = new System.Drawing.Point(256, 165);
            this.dateTimePicker3.Name = "dateTimePicker3";
            this.dateTimePicker3.Size = new System.Drawing.Size(237, 20);
            this.dateTimePicker3.TabIndex = 43;
            // 
            // textBox3
            // 
            this.textBox3.Location = new System.Drawing.Point(323, 118);
            this.textBox3.Name = "textBox3";
            this.textBox3.Size = new System.Drawing.Size(128, 20);
            this.textBox3.TabIndex = 7;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Location = new System.Drawing.Point(105, 121);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(185, 13);
            this.label24.TabIndex = 6;
            this.label24.Text = "Enter Appointment Reference number";
            // 
            // TimeMgmtSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(742, 496);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Name = "TimeMgmtSystem";
            this.Text = "Time Management Software 1.0";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.Home.ResumeLayout(false);
            this.Home.PerformLayout();
            this.RegApp.ResumeLayout(false);
            this.tabControl2.ResumeLayout(false);
            this.tabPage6.ResumeLayout(false);
            this.tabPage6.PerformLayout();
            this.tabPage7.ResumeLayout(false);
            this.tabPage7.PerformLayout();
            this.tabPage8.ResumeLayout(false);
            this.tabPage8.PerformLayout();
            this.Arrange.ResumeLayout(false);
            this.Arrange.PerformLayout();
            this.ChkSch.ResumeLayout(false);
            this.ChkSch.PerformLayout();
            this.Update.ResumeLayout(false);
            this.tabControl3.ResumeLayout(false);
            this.tabPage9.ResumeLayout(false);
            this.tabPage9.PerformLayout();
            this.tabPage10.ResumeLayout(false);
            this.tabPage10.PerformLayout();
            this.tabPage11.ResumeLayout(false);
            this.tabPage11.PerformLayout();
            this.Stat.ResumeLayout(false);
            this.tabControl4.ResumeLayout(false);
            this.tabPage12.ResumeLayout(false);
            this.tabPage13.ResumeLayout(false);
            this.tabPage14.ResumeLayout(false);
            this.tabPage15.ResumeLayout(false);
            this.Slot.ResumeLayout(false);
            this.Slot.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown6)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown8)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label welcome;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button logout;
        private System.Windows.Forms.Button seclogin;
        private System.Windows.Forms.Button exelogin;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage RegApp;
        private System.Windows.Forms.TabPage Arrange;
        private System.Windows.Forms.TabPage ChkSch;
        private System.Windows.Forms.TabPage Update;
        private System.Windows.Forms.TabPage Stat;
        private System.Windows.Forms.TabControl tabControl2;
        private System.Windows.Forms.TabPage tabPage6;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.TextBox ppltxtbox;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox venuetxtbox;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.DateTimePicker appdate;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TabPage tabPage7;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TabPage tabPage8;
        private System.Windows.Forms.Label appnumlabel;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.ProgressBar progressBar1;
        private System.Windows.Forms.MonthCalendar monthCalendar1;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Button button10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabControl tabControl3;
        private System.Windows.Forms.TabPage tabPage9;
        private System.Windows.Forms.MonthCalendar leaveenddate;
        private System.Windows.Forms.MonthCalendar leavestartdate;
        private System.Windows.Forms.TabPage tabPage10;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Button submitbutton;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button button12;
        private System.Windows.Forms.DateTimePicker dateTimePicker2;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TabPage tabPage11;
        private System.Windows.Forms.Button button13;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Button button14;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TabControl tabControl4;
        private System.Windows.Forms.TabPage tabPage12;
        private System.Windows.Forms.TabPage tabPage13;
        private System.Windows.Forms.TabPage Home;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.WebBrowser webBrowser1;
        private System.Windows.Forms.TabPage tabPage14;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TabPage tabPage15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TabPage Slot;
        private System.Windows.Forms.Label label31;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button button17;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.NumericUpDown numericUpDown5;
        private System.Windows.Forms.NumericUpDown numericUpDown6;
        private System.Windows.Forms.NumericUpDown numericUpDown7;
        private System.Windows.Forms.NumericUpDown numericUpDown8;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Label label27;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.DateTimePicker dateTimePicker3;
        private System.Windows.Forms.TextBox textBox3;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox ettxtbox;
        private System.Windows.Forms.TextBox sttxtbox;
        private System.Windows.Forms.Button arrangebutton;
    }
}

