﻿CREATE TABLE [dbo].[Appointment] (
    [uname]     NCHAR (10) NOT NULL,
    [date]      DATE       NULL,
    [starttime] NCHAR(4)        NULL,
    [endtime]   NCHAR(4)        NULL,
    [venue]     NCHAR (10) NULL,
    [ppl]       INT        NULL,
    [appno]     INT        NULL,
    PRIMARY KEY CLUSTERED ([uname] ASC)
);

